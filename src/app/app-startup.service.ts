import { Inject, Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { first, mergeMap, tap } from 'rxjs/operators';

import { appConfigToken } from './app.di';
import { AppConfig } from './app.model';

@Injectable()
export class AppStartupService {

  constructor(
    private translate: TranslateService,
    @Inject(appConfigToken) private appConfig: AppConfig,
    private meta: Meta,
    private title: Title
  ) {}

  load(): Observable<any> {
    this.meta.addTag({
      name: 'app:version',
      content: this.appConfig.version
    });

    const lang = 'en';
    this.translate.setDefaultLang(lang);
    return this.translate.use(lang).pipe(first(), mergeMap(() => {
      return this.translate.get('app.title').pipe(tap((title) => {
        this.title.setTitle(title);
      }));
    }));
  }

}

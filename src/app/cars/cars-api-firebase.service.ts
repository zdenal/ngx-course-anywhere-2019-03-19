import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { assign, interpolate } from '../utils/common.utils';
import { CarsApiService } from './cars-api.service';
import { carsConfigToken } from './cars.di';
import { Car, CarData, Cars, CarsConfig, CarsResponse } from './cars.model';

@Injectable()
export class CarsApiFirebaseService extends CarsApiService {

  constructor(
    @Inject(carsConfigToken) private config: CarsConfig,
    private http: HttpClient
  ) {
    super();
  }

  getCars$(): Observable<Cars> {
    return this.http.get(this.config.getCarsUrl).pipe(map((res: CarsResponse) => {
      return res ? Object.keys(res).map((id) => {
        return { id, ...res[id] };
      }) : [];
    }));
  }

  getCar$(id: string): Observable<Car> {
    return this.http.get(interpolate(this.config.getCarUrl, { id })).pipe(map((res: CarData) => {
      if (!res) {
        throw new Error(`Car with id ${id} not exists.`);
      }
      return { id, ...res };
    }));
  }

  updateCar$(car: Car): Observable<Car> {
    car = assign(car);
    const id = car.id;
    delete car.id;
    return this.http.put(interpolate(this.config.updateCarUrl, { id }), car).pipe(map((res: CarData) => {
      return { id, ...res };
    }));
  }

  createCar$(car: Car): Observable<Car> {
    delete car.id;
    return this.http.post(this.config.createCarUrl, car).pipe(mergeMap(({ name: id }: { name: string; }) => {
      return this.getCar$(id);
    }));
  }

  deleteCar$(id: string): Observable<any> {
    return this.http.delete(interpolate(this.config.deleteCarUrl, { id })).pipe(map(() => null));
  }

}
